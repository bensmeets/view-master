if (Meteor.isClient) {

  /**
    Our current method for creating a viewstack-like
    behaviour in Meteor. Uses all the default technique
    (read: noobie-stuff) that is provided by Meteor.

    This should somehow be abstracted in a seperate class,
    since a lot of mechanics of creating/destroying viewstack
    elements should better be grouped together. I currently
    have no clue whatsoever how that can be accomplished.

    An initial attempt is in the "libs" directory, but not
    even close to complete. All these questions.... like:
    - How can we use that lib thingie in the template?
    - How do we tell that thingie to loop over stuff (without
      having "each" to help us I think?)
    - Etc...

    Food for thought...

  */


  /**
    The rest of our helpers to help us fill the templates
    with some usefull testdata. Does nothing special.
  */
  Template.customers.helpers({
    customers: function() {
      return customers.find();
    }
  });

  Template.customer.helpers({
    customer: function() {
      return customers.findOne({_id:this.key});
    },
    invoices: function() {
      return invoices.find();
    }
  });


  Template.invoice.helpers({
    customer: function() {
      return customers.findOne();
    }
  });
}