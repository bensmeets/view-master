/**
 * Combines all functionality regarding a single viewstach
 * inside the application. Will register the correct (client
 * only) Collections, handle it's events and manage creating
 * and destroying new or old views inside the viewstack.
 */

var viewStackItems = new Meteor.Collection(null);
viewStackItems.insert({ template:'customers' });

Template.viewstack.helpers({
  'views': function() {
    return viewStackItems.find();
  }
});


/**
 * Catch global viewstack events, mostly closing, adding, etc. of panels
 */
Template.viewstack.events({
  'click .hotlink': function(e, tpl) {

    var t, el = $(e.target);

    if (el.hasClass('hotlink-customer'))
      t = 'customer';
    else if (el.hasClass('hotlink-customers'))
      t = 'customers';
    else if (el.hasClass('hotlink-invoice'))
      t = 'invoice';
    else if (el.hasClass('hotlink-invoices'))
      t = 'invoices';

    viewStackItems.insert({ template: t, key: this._id });
  },
  'click .close': function(e, tpl) {
    viewStackItems.remove(this._id);
  }
});